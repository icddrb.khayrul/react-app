import React, {Component} from 'react'

import './Title.css'

class Title extends Component {

    constructor(props){
        super(props);

        this.state = {
            title: 'This is dummy text!',
            isInput: false
        }
    }

    editHandaler() {
        this.setState({
            ...this.state,
            isInput: true
        })
    }

    inputChange(event) {
        this.setState({
            ...this.state,
            title:event.target.value
        })

    }

    keyPressHandeler(event) {
        if(event.key === 'Enter'){
            this.setState({
                ...this.state,
                isInput: false
            })
        }
    }

    blueHandeler(){
        this.setState({
            ...this.state,
            isInput:false
        })
    }

    

    render() {
        let output = null

        if(this.state.isInput) {
            output = <input 
                        className="form-control" 
                        onChange={ (event)=>this.inputChange(event)}
                        onKeyPress={ (event)=>this.keyPressHandeler(event) }
                        onBlur= { (event)=>this.blueHandeler(event) }
                        type="text" 
                        value={ this.state.title} 
                    />
        } else {
            output = (
                <div className='d-flex title'>
                    <h1 className='display-4'>{this.state.title}</h1>
                    <span onClick={ ()=>this.editHandaler() } className='ml-auto edit-icon'>
                        <i className='fas fa-pencil-alt'></i>
                    </span>
                </div>
            )
        }


        return(
            <div>
                {output}
            </div>
        )
    }

}

export default Title